import { ElementType, lazy, Suspense } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import LoadingScreen from '../components/LoadingScreen';
import MainLayout from '../layouts/MainLayout';
import Dashboard from '../pages/Dashboard';
import InvoiceHistory from '../pages/InvoiceHistory';

const Loadable = (Component: ElementType) => (props: any) => {

  return (
    <Suspense
      fallback={<LoadingScreen />}
    >
      <Component {...props} />
    </Suspense>
  );
};

const NotFound = Loadable(lazy(() => import('../pages/Page404')));


export default function Router() {
  return useRoutes([
    
    {
      path: '/',
      element: (
          <MainLayout />
      ),
      children: [
        { element: <Navigate to="/dashboard" replace />, index: true },
        { path: 'dashboard', element: <Dashboard /> },
        { path: 'invoice-history', element: <InvoiceHistory /> },
        { path: '404', element: <NotFound /> },
      ],
    },
    { path: '*', element: <Navigate to="/404" replace /> },
  ]);
}
