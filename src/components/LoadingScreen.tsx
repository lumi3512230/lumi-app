import { CircularProgress } from '@mui/material';

export default function LoadingScreen() {
    return <div><CircularProgress/></div>
}